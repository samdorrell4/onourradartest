import React, { FC, useEffect, useLayoutEffect, useRef, useState } from 'react';
import { Message, useMessagesQuery } from '../../graphql/generated/graphql';
import { useInView } from "react-intersection-observer";


const LIMIT = 100;
const START = 5;
const INCREMENT = 3;

const timeFormatter = (timestamp: string): string => {
	const date = new Date(timestamp);
	const dateString = date.toLocaleDateString('en-GB', {
		day: 'numeric',
		month: 'numeric',
		year: 'numeric',
	});
	const timeString = date.toLocaleTimeString('UTC');
	return `${dateString} ${timeString} UTC`;
};

const AMessage: FC<Message> = ({ id, timestamp, message }): JSX.Element => {
	return (
		<div className="border-2 rounded-xl p-4 my-2 w-1/4 bg-gray-400">
			<div className="grid grid-cols-3">
				<div className="col-start-1 text-xs text-gray-600 col-end-4">
					{timeFormatter(timestamp)}
				</div>
				<div className="col-span-3">{message}</div>
			</div>
		</div>
	);
};

const Messages: FC = () => {

	const [nMessages, setNMessages] = useState(START);
	const [scrollPos, setScrollPos] = useState(0);
	const messagesRef = useRef(null)

  const [ref, inView] = useInView({
    threshold: 1
  });

	const { data } = useMessagesQuery({
		variables: {
			limit: nMessages,
			lastTimestamp: null,
		},
	});

	const loadMoreMessages = () => {
		if (nMessages === LIMIT) return;
		setNMessages(Math.min(nMessages + INCREMENT,LIMIT));
	}

	useEffect(() => {
		if (inView){
			loadMoreMessages()
		}
	}, [inView]);

	useLayoutEffect(() => {
    const div = messagesRef.current;
		if (div) {
			console.log("restore scrollPos: %s",scrollPos)
		}
	}, [nMessages,messagesRef]);
	

	const scrollFunc = (e:any)=>{
		setScrollPos(e.target.scrollTop)
	}

	return (
		<>
			<hr />
			<div>Messages</div>
			{/* <p>{inView ? "in view" : "not in view"}</p> */}
			<hr />
			<div className="h-64 overflow-x-hidden overflow-y-scroll bg-gray-100" ref={messagesRef} onScroll={scrollFunc}>
				{data?.messages.map((message,index) => {
					const lastMessage = index === nMessages-1;
					return (
						<div key={message.id} ref={lastMessage ? ref : null} style={{display:"inline"}}>
							<AMessage key={message.id} {...message} />
						</div>	
					)
				})}
			</div>
		</>
	);
};

export default Messages;
