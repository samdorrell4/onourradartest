import React, { FC } from 'react';
import { Messages } from '../../components';

const Task: FC = () => (
	<div className="text-left">
		<p className="font-bold">The task</p>
		<p>
			The task is to make the message box below load on scroll using graphql
			queries
		</p>
		<p>
			graphql-code-generator is being used to generate react hooks from the .schema
			and .graphql file
		</p>
		<p>
			It only needs to run on modern browsers so using an IntersectionObserver
			is fine
		</p>
		<p>
			There are 100 messages provided by the graphql endpoint, there is never an
			identical timestamp
		</p>
		<p>The api serves messages latest first</p>
		<p>The api lastTimestamp parameter can be used to return messages below the given timestamp</p>
		<p>LIMIT will never be defined to be below 2 (defined in messages.tsx)</p>
		<Messages />
	</div>
);

export default Task;
