import Home from './home';
import About from './about';
import Scripts from './scripts';
import Task from './task';

export { Home, About, Scripts, Task };
