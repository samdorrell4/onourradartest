import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `DateTime` scalar type represents a date and time in the UTC
   * timezone. The DateTime appears in a JSON response as an ISO8601 formatted
   * string, including UTC timezone ("Z"). The parsed date and time string will
   * be converted to UTC if there is an offset.
   */
  DateTime: any;
};


/** A Message */
export type Message = {
  readonly __typename?: 'Message';
  readonly id: Scalars['ID'];
  readonly message?: Maybe<Scalars['String']>;
  readonly timestamp: Scalars['DateTime'];
};

export type RootQueryType = {
  readonly __typename?: 'RootQueryType';
  /** Get all messages ordered by timestamp descending using the limit and last_timestamp fields, when last_timestamp is null the latest messages are returned */
  readonly messages: ReadonlyArray<Message>;
};


export type RootQueryTypeMessagesArgs = {
  limit: Scalars['Int'];
  lastTimestamp?: Maybe<Scalars['DateTime']>;
};

export type MessagesQueryVariables = Exact<{
  limit: Scalars['Int'];
  lastTimestamp?: Maybe<Scalars['DateTime']>;
}>;


export type MessagesQuery = { readonly __typename?: 'RootQueryType', readonly messages: ReadonlyArray<{ readonly __typename?: 'Message', readonly id: string, readonly timestamp: any, readonly message?: Maybe<string> }> };


export const MessagesDocument = gql`
    query Messages($limit: Int!, $lastTimestamp: DateTime) {
  messages(limit: $limit, lastTimestamp: $lastTimestamp) {
    id
    timestamp
    message
  }
}
    `;

/**
 * __useMessagesQuery__
 *
 * To run a query within a React component, call `useMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMessagesQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      lastTimestamp: // value for 'lastTimestamp'
 *   },
 * });
 */
export function useMessagesQuery(baseOptions: Apollo.QueryHookOptions<MessagesQuery, MessagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MessagesQuery, MessagesQueryVariables>(MessagesDocument, options);
      }
export function useMessagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MessagesQuery, MessagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MessagesQuery, MessagesQueryVariables>(MessagesDocument, options);
        }
export type MessagesQueryHookResult = ReturnType<typeof useMessagesQuery>;
export type MessagesLazyQueryHookResult = ReturnType<typeof useMessagesLazyQuery>;
export type MessagesQueryResult = Apollo.QueryResult<MessagesQuery, MessagesQueryVariables>;