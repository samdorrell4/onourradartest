import {
  ApolloClient,
  InMemoryCache,
} from "@apollo/client";

const cache = new InMemoryCache({})

export const client = new ApolloClient({
  uri: 'https://fe-dev-test-server.onrender.com/api',
  cache: cache,
  connectToDevTools: true,
});
