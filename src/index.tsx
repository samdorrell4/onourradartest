import React, { FC } from 'react';
import ReactDOM from 'react-dom';
import "./index.css";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { client } from "./graphql/apolloClient";
import * as serviceWorker from './serviceWorker';

import { Home, About, Scripts, Task } from './pages';
import { Layout } from './containers';

export const App: FC = () => (
	<Router>
		<Layout>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/about" component={About} />
				<Route path="/scripts" component={Scripts} />
				<Route path="/task" component={Task} />
			</Switch>
		</Layout>
	</Router>
);

ReactDOM.render(
	<ApolloProvider client={client}>
		<App />
	</ApolloProvider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
